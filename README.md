# Funding

One Step Relief Project - Funding Module

The Fund Module facilitates all of the crowdfunding and general funding functions within the Relief Platform, including creating a relief project fund and the reception and distribution of funds. The module allows for the creation, update and management of Relief Funding Projects, which represent a fundraising goal for a designated purpose; concrete examples could include building a new school or purchasing food/beds for disaster stricken areas. 
